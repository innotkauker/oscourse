#include <inc/assert.h>
#include <inc/x86.h>
#include <kern/spinlock.h>
#include <kern/env.h>
#include <kern/pmap.h>
#include <kern/monitor.h>
#include <kern/kclock.h>
#include <inc/env.h>
#include <kern/sched-head.h>
#include <kern/alloc.h>
#include <kern/picirq.h>

static void scheduler(void);

struct Taskstate cpu_ts;
void sched_halt(void);

// main scheduler lists
ListElement *loaded_envs;
ListElement *fast_envs;
ListElement *slow_envs;
unsigned loaded_envs_len, fast_envs_len, slow_envs_len;

uint64_t fast_slow_border = DEF_ASSUMED_TIME;

// different timing variables
uint64_t started_at_tsc;
uint64_t cur_tsc;
uint64_t last_interrupt_timing;
uint64_t cur_env_wants_t = 0, cur_env_runs_for = 0;
uint64_t time_since_last_amnesty = 0;

// curenv parameters
bool cur_has_finished = false;
bool suspicious_behaviour = false;

// phase handling
bool fast_envs_only = true;
uint64_t current_phase_time = 0, slow_env_step = 0, slow_env_eps;
int slow_env_i = -1, slow_envs_n = 0, slow_env_launch = -1;
ListElement *cur_slow_env = NULL;
uint64_t cur_slow_running_time = 0;

// becomes true when we select first env and start timer
bool initialized = false; 

// to run all envs before sorting them
bool everyone_was_run = false;
int envs_run = 0;

// processor frequency estimation
uint64_t clock_cycles_per_usec = 0;
uint64_t clock_accumulator = 0;  // these are for measuring 
unsigned measure_num = 0;		// clock_cycles_per_usec avg
uint64_t clock_tsc;

#ifdef CONFIG_KSPACE
static void
perform_io_simulation( void )
{
	if (in_clock_interrupt()) {
		int i;
		for(i = 0; i < NENV; ++i) {
			if (envs[i].env_status == ENV_NOT_RUNNABLE) {
				if (!(--envs[i].blocking_cycles))
					envs[i].env_status = ENV_RUNNABLE;
			}
		}
	}
}
#endif

void
sched_yield(void)
{
#ifdef CONFIG_KSPACE
	perform_io_simulation();
#endif
	cur_tsc = read_tsc();
	scheduler();
}

int64_t 
cmp_env_a_time(ListElement *a, ListElement *b) 
{
	return a->env->assumed_time - b->env->assumed_time;
}

bool 
env_a_time_gt_MFPRT(ListElement *a) 
{
	return a->env->assumed_time > MAX_FAST_PROC_REQ_T;
}

bool 
env_w_time_gt(ListElement *cur, ListElement *extr) 
{
	return (cur->env->time_waiting > extr->env->time_waiting);
}

int64_t 
cmp_env_w_time_r(ListElement *a, ListElement *b) 
{
	return b->env->time_waiting - a->env->time_waiting;
}

int 
make_env_lists() 
{
	// free these lists
	log_printf(1, "making lists\n");
	free_list(fast_envs, &fast_envs_len);
	free_list(slow_envs, &slow_envs_len);
	loaded_envs = list_sort(loaded_envs, cmp_env_a_time); 
	log_printf(0, "sorted loaded_envs:\n");
	output_list(0, loaded_envs);
	ListElement *search_border = list_find(loaded_envs, env_a_time_gt_MFPRT);
	if (!search_border || search_border == loaded_envs) { 
		// work with the entire array
		search_border = NULL;
	}
	ListElement *fast_border = list_find_border(loaded_envs, search_border, cmp_env_a_time);
	fast_envs = make_list(loaded_envs, fast_border, &fast_envs_len, true);
	slow_envs = make_list(fast_border->next, NULL, &slow_envs_len, false);
	fast_slow_border = 0.5*fast_border->env->assumed_time;
	if (fast_border->next)
		fast_slow_border += 0.5*fast_border->next->env->assumed_time;
	else
		fast_slow_border *= 2;

	log_printf(1, "fast_envs:\n");
	output_list(1, fast_envs);
	log_printf(1, "slow_envs:\n");
	output_list(1, slow_envs);
	return 0;
}

void
conf_slow_envs_for_phase(void)
{
	log_printf(2, "started handling slow envs\n");
	slow_envs_n = slow_envs_len;
	if (slow_envs_n) {
		slow_env_step = PHASE_TIME/2/slow_envs_n;
		slow_env_eps = slow_env_step/SLOW_PROC_EMBEDDED_STEPS;
		slow_env_i = 0;
		slow_env_launch = 0;
		slow_envs = list_sort(slow_envs, cmp_env_w_time_r);
		cur_slow_env = slow_envs;
	} else {
		// if there are currently no slow envs, start new phase?
		// no.
	}
}

void 
select_next_slow_env(void)
{ 
	// select next runnable slow env
	if (!cur_slow_env)
		return;

	cur_slow_env = cur_slow_env->next;

	while (cur_slow_env && !is_runnable(cur_slow_env))
		cur_slow_env = cur_slow_env->next;

	if (cur_slow_env) {
		slow_env_i++;
		slow_env_launch = 0;
		log_printf(1, "next slow env, %u\n", slow_env_i);
	}
}

void
find_any_slow_env(void)
{
	ListElement *slow = slow_envs;
	if (!slow_envs) {
		log_printf(1, "no slow envs in select_env()\n");
		sched_halt();
	}

	while (slow && !is_runnable(slow))
		slow = slow->next;

	if (!slow) {
		log_printf(1, "no runnable slow envs in select_env()\n");
		sched_halt();
	}
	run_env(slow->env);
}

void
find_waiting_fast_env(void)
{
	ListElement *fast = list_find_extreme(fast_envs, env_w_time_gt, true);

	if (!everyone_was_run) {
		envs_run++;
	}
	
	if (fast) {
		run_env(fast->env); 
	} else {
		log_printf(1, "no runnable fast envs in select_env()\n");
		// select a slow one somehow
		find_any_slow_env();
	}
}

void
select_env(void) 
{
	if (!everyone_was_run && envs_run == loaded_envs_len) {
		make_env_lists(); // finally
		everyone_was_run = true;
	}

	log_printf(0, "fast_envs:\n");
	output_list(0, fast_envs);
	log_printf(0, "slow_envs:\n");
	output_list(0, slow_envs);


	if ((current_phase_time) > PHASE_TIME) {
		// current phase has ended, start a new one
		log_printf(2, "new phase has started, cpt is %u\n", current_phase_time);
		current_phase_time -= PHASE_TIME;
		slow_env_i = -1;
		make_env_lists(); 
	} else if (current_phase_time >= PHASE_TIME/2) {
		
		if (slow_env_i == -1) // configure slow envs handling for this phase
			conf_slow_envs_for_phase();

		if (slow_envs_n) { // there are slow envs 
			if (current_phase_time >= PHASE_TIME/2 + slow_env_i*slow_env_step 
					+ slow_env_launch*slow_env_eps) { // time to launch one of them
				
				if (slow_env_launch >= SLOW_PROC_EMBEDDED_STEPS) // switch to the next slow env 
					select_next_slow_env();

				if (cur_slow_env && cur_slow_env->env->env_status != ENV_FREE) {
					slow_env_launch++;
					log_printf(0, "launch slow env for %d' time\n", slow_env_launch);
					run_env(cur_slow_env->env);
				} // else => handle fast envs
			}
		}
	}

	// select fast env with the highest waiting time
	find_waiting_fast_env(); // and run it
}

void
run_env(struct Env *env)
{
	if (!env) // nothing runnable
		sched_halt();

	uint64_t delta = env->assumed_time /*- env->total_time_active */- env->time_active;
	cur_env_wants_t = rtc_set_int_rate(delta); // doesn't set anything actually
	cur_env_runs_for = 0;
	cur_has_finished = false;
	suspicious_behaviour = false;

	// and run
	log_printf(3, "env %u is runnable for %u msec\n", ENVX(env->env_id), cur_env_wants_t);
	if (!initialized) {
		initialized = true;
		rtc_init();
	}
	// if (clock_cycles_per_usec) { // add time spent by scheduler?
	// 	current_phase_time += (read_tsc() - cur_tsc)/clock_cycles_per_usec;
	// }
	started_at_tsc = read_tsc();
	// pic_send_eoi(rtc_check_status());
	env_run(env);
}

void 
setup_cpu_freq() 
{
	// setting clock_cycles_per_usec
	if (measure_num < AVG_CLOCK_MEASUREMENTS_N) {
		clock_cycles_per_usec = (cur_tsc - clock_tsc) / last_interrupt_timing;
		clock_accumulator += clock_cycles_per_usec;
		log_printf(1, "cpu performance measurement result: %u \n", clock_cycles_per_usec);
		if (++measure_num >= AVG_CLOCK_MEASUREMENTS_N) {
			clock_cycles_per_usec = clock_accumulator / measure_num;
			log_printf(2, "final clock_cycles_per_usec: %u\n", clock_cycles_per_usec);
		}
	}
	clock_tsc = cur_tsc;
}

uint64_t 
get_spent_time()
{
	// the running time must always be estimated as we can't stop the timer
	if (clock_cycles_per_usec) {
		return (cur_tsc - started_at_tsc)/clock_cycles_per_usec;
	} else { // if the denuminator is not set, just skip everything
		return 0;
	}
}

void 
distribute_amnesties()
{
	time_since_last_amnesty = 0;
	ListElement *i = loaded_envs; 
	while (i) {
		if (i->env->penalty >= AMNESTY_POINTS)
			i->env->penalty -= AMNESTY_POINTS;
		else if (i->env->penalty > 0)
			i->env->penalty = 0;
		i = i->next;
	}
}

void 
slow_to_fast_to_slow()
{
	ListElement *t;
	if (curenv->fast && curenv->assumed_time > 1.1*fast_slow_border) {
		log_printf(1, "turning env %u from fast to slow\n", ENVX(curenv->env_id));
		curenv->fast = false;
		push_before_list(&slow_envs, curenv, &slow_envs_len);
		// remove_from_list(&fast_envs, curenv, &fast_envs_len);

		if ((t = list_find_element(fast_envs, curenv))) {
			remove_from_list(&fast_envs, t, &fast_envs_len);
			if (fast_envs_len < 2)
				make_env_lists();
		} else { // that is bad
			log_printf(2, "failed to find fast env in fast_env list\n");
		}
	} else if (!curenv->fast && curenv->assumed_time < 0.9*fast_slow_border) {
		log_printf(1, "turning env %u from slow to fast\n", ENVX(curenv->env_id));
		curenv->fast = true;
		// remove_from_list(&slow_envs, &curenv, &slow_envs_len);
		push_before_list(&fast_envs, curenv, &fast_envs_len);
		if ((t = list_find_element(slow_envs, curenv))) {
			remove_from_list(&slow_envs, t, &slow_envs_len);
		} else { // that is also bad
			log_printf(2, "failed to find slow env in slow_env list\n");
		}
	}
}

void 
deal_with_curenv(uint64_t time_spent)
{
	int64_t cur_delta;
	// deal with the process that has just been stopped
	if (curenv) {
		log_printf(0, "env %u spent: %u\n", ENVX(curenv->env_id), time_spent);
		log_printf(0, "assumed: %u\n", curenv->assumed_time);

		curenv->total_time_active += time_spent;
		curenv->time_waiting = 0;
		curenv->time_active += time_spent;

		if (time_spent < 2) {
			// just in case we don't have any cpu freq measurements yet
			return;
		}

		if (!in_clock_interrupt()) { 
			// we are ready to make some measurements
			// as the process has returned control itself
			if (curenv->default_timing) {
				curenv->assumed_last_time = 
						WEIGHTING_FACTOR * curenv->time_active +
							(1 - WEIGHTING_FACTOR) * curenv->assumed_last_time;
				curenv->assumed_time = curenv->total_time_active + curenv->assumed_last_time;
			} else if (suspicious_behaviour) {
				if (cur_env_runs_for >= 0.8*cur_env_wants_t) {
					// second banning condition
					if (++(curenv->penalty) > MAX_PENALTY) { // and we are sure about it
						curenv->blocked = true;
						curenv->default_timing = true;
						log_printf(2, "blocking process %u\n", ENVX(curenv->env_id));
					}
				
				}
			}
			curenv->time_active = 0;
		}
		if (!curenv->default_timing &&
			 (cur_delta = curenv->assumed_time - curenv->total_time_active) < 0) {
				// process has exceeded its assumed running time
				// it's always user fault
				// well, if we occasionally start measuring time adeqately
				if (-cur_delta > MAX_ALLOWED_DELTA_PERCENT*last_interrupt_timing) {
					// the process is trying to steal time
					if (++(curenv->penalty) > MAX_PENALTY) { // and we are sure about it
						curenv->blocked = true;
						curenv->default_timing = true;
						log_printf(2, "blocking process %u\n", ENVX(curenv->env_id));
					} // else => no.
					curenv->assumed_time = curenv->total_time_active + 
												converter[default_timer_rate_id].time;
				}	
			}
#ifdef FTSTF
		slow_to_fast_to_slow();
#endif 
	} else {
		log_printf(1, "no curenv!\n"); 
	}
}

void 
add_waiting_time(uint64_t time_spent)
{
	ListElement *i = loaded_envs;
	if (!curenv)
		return; // well, ...
	for (; i; i = i->next) {
		if ((i->env != curenv) && is_runnable(i))
			i->env->time_waiting += time_spent;
		// if ((i->env == curenv))
		// 	cprintf("curenv in awt()");
	}
}

// Choose a user environment to run and run it.
static void
scheduler(void)
{ 
	// irq_setmask_8259A(0xFEFF); // disable clock interrupts
	if (initialized) {
		// turn_off_the_oscillator(); // i wish it worked
		// last_tsc = cur_tsc;
		uint64_t time_spent = get_spent_time();

		if (!time_spent) // frequency not estimated yet
			time_spent = 1;

		cur_env_runs_for += time_spent;
		if (curenv && !cur_has_finished 
				&& cur_env_runs_for < cur_env_wants_t) { // curenv needs more time
			// irq_setmask_8259A(0xFEFB);	
			log_printf(0, "continuing\n");

			env_run(curenv);
		} // else => select another one
		
		if ((time_since_last_amnesty += cur_env_runs_for) >= TIME_BETWEEN_AMNESTIES) {
			distribute_amnesties();
		}
		
		add_waiting_time(cur_env_runs_for);
		deal_with_curenv(cur_env_runs_for);

		// current_phase_time += cur_env_runs_for;
		log_printf(1, "last env has run for %d\n", cur_env_runs_for);
	} // else => this is the first scheduler launch
	select_env(); // and run it
}


// Halt this CPU when there is nothing to do. Wait until the
// timer interrupt wakes it up. This function never returns.
//
void
sched_halt(void)
{

	int i;

	for(i = 0; i < NENV; ++i) {
		if (envs[i].env_status == ENV_NOT_RUNNABLE) {
			envs[i].env_status = ENV_RUNNABLE;
			sched_yield();
		}
	}

	// For debugging and testing purposes, if there are no runnable
	// environments in the system, then drop into the kernel monitor.
	for (i = 0; i < NENV; i++) {
		if ((envs[i].env_status == ENV_RUNNABLE ||
			 envs[i].env_status == ENV_RUNNING))
			break;
	}
	if (i == NENV) {
		log_printf(2, "No runnable environments in the system!\n");
		while (1)
			monitor(NULL);
	}

	// Mark that no environment is running on CPU
	curenv = 0;

	// Reset stack pointer, enable interrupts and then halt.
	asm volatile (
		"movl $0, %%ebp\n"
		"movl %0, %%esp\n"
		"pushl $0\n"
		"pushl $0\n"
		"sti\n"
		"hlt\n"
	: : "a" (cpu_ts.ts_esp0));
}