// Simple command-line kernel monitor useful for
// controlling the kernel and exploring the system interactively.

#include <inc/stdio.h>
#include <inc/string.h>
#include <inc/memlayout.h>
#include <inc/assert.h>
#include <inc/x86.h>

#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/kdebug.h>
#include <kern/trap.h>

#include <kern/pmap.h>

#define CMDBUF_SIZE	80	// enough for one VGA text line



struct Command {
	const char *name;
	const char *desc;
	// return -1 to force monitor to exit
	int (*func)(int argc, char** argv, struct Trapframe* tf);
};

static struct Command commands[] = {
	{ "help", "Display this list of commands", mon_help },
	{ "kerninfo", "Display information about the kernel", mon_kerninfo },
	{ "hello", "Say \"Hello!\"", mon_hello },
	{ "backtrace", "Stack backtrace" , mon_backtrace },
	{ "pages", "Show pages" , mon_pages }
};
#define NCOMMANDS (sizeof(commands)/sizeof(commands[0]))

/***** Implementations of basic kernel monitor commands *****/
int
mon_pages(int argc, char **argv, struct Trapframe *tf)
{
	int i=0;
	int fr=0, al=0;
	struct PageInfo *p=pages;
	
	cprintf("Npages: %d\n", npages);

	while (i < npages) {
		if (p[i].pp_ref==0) {
			cprintf("%d..",i+1);
			i++;
			while (i < npages && p[i].pp_ref==0) {
				i++;				
			}
			cprintf("%d FREE\n",i);			
		}
		if (p[i].pp_ref>=1) {
			cprintf("%d..",i+1);
			i++;
			while (i < npages && p[i].pp_ref>=1) {
				i++;
			}
			cprintf("%d ALLOCATED\n",i);
		}				
	}

	return 0;
}

int
mon_hello(int argc, char **argv, struct Trapframe *tf)
{
	cprintf("%s \n", "Hello!");
	return 0;
}

int
mon_help(int argc, char **argv, struct Trapframe *tf)
{
	int i;

	for (i = 0; i < NCOMMANDS; i++)
		cprintf("%s - %s\n", commands[i].name, commands[i].desc);
	return 0;
}

int
mon_kerninfo(int argc, char **argv, struct Trapframe *tf)
{
	extern char _start[], entry[], etext[], edata[], end[];

	cprintf("Special kernel symbols:\n");
	cprintf("  _start                  %08x (phys)\n", _start);
	cprintf("  entry  %08x (virt)  %08x (phys)\n", entry, entry - KERNBASE);
	cprintf("  etext  %08x (virt)  %08x (phys)\n", etext, etext - KERNBASE);
	cprintf("  edata  %08x (virt)  %08x (phys)\n", edata, edata - KERNBASE);
	cprintf("  end    %08x (virt)  %08x (phys)\n", end, end - KERNBASE);
	cprintf("Kernel executable memory footprint: %dKB\n",
		ROUNDUP(end - entry, 1024) / 1024);
	return 0;
}

int
mon_backtrace(int argc, char **argv, struct Trapframe *tf)
{
	// Your code here.
	uint32_t ebp, eip;
	int i, k;
	uint32_t *p,*bp;
	struct Eipdebuginfo info;
	
	cprintf("Stack backtrace:\n");	
	ebp=read_ebp();
	p=(uint32_t *)ebp;

	while (p) {		
		bp=p;
		bp++;		
		eip=*bp;
		k=debuginfo_eip(eip, &info);

		cprintf("  ebp %08x ", ebp);
		cprintf("eip %08x args ", *(bp++));
		for (i=0; i<5; bp++,i++) 
			cprintf("%08x ", *bp);
		cprintf("\n");

		// if (k) {...}
		if (1) {
			cprintf("%s:%d: ", info.eip_file, info.eip_line);
			cprintf("%.*s", info.eip_fn_namelen, info.eip_fn_name);
			cprintf("+%d\n",eip-info.eip_fn_addr);
		}

		ebp=*p; 
		p=(uint32_t *)ebp;
	}
		
	return 0;
}


/***** Kernel monitor command interpreter *****/

#define WHITESPACE "\t\r\n "
#define MAXARGS 16

static int
runcmd(char *buf, struct Trapframe *tf)
{
	int argc;
	char *argv[MAXARGS];
	int i;

	// Parse the command buffer into whitespace-separated arguments
	argc = 0;
	argv[argc] = 0;
	while (1) {
		// gobble whitespace
		while (*buf && strchr(WHITESPACE, *buf))
			*buf++ = 0;
		if (*buf == 0)
			break;

		// save and scan past next arg
		if (argc == MAXARGS-1) {
			cprintf("Too many arguments (max %d)\n", MAXARGS);
			return 0;
		}
		argv[argc++] = buf;
		while (*buf && !strchr(WHITESPACE, *buf))
			buf++;
	}
	argv[argc] = 0;

	// Lookup and invoke the command
	if (argc == 0)
		return 0;
	for (i = 0; i < NCOMMANDS; i++) {
		if (strcmp(argv[0], commands[i].name) == 0)
			return commands[i].func(argc, argv, tf);
	}
	cprintf("Unknown command '%s'\n", argv[0]);
	return 0;
}

void
monitor(struct Trapframe *tf)
{
	char *buf;

	cprintf("Welcome to the JOS kernel monitor!\n");
	cprintf("Type 'help' for a list of commands.\n");

	if (tf != NULL)
		print_trapframe(tf);

	while (1) {
		buf = readline("K> ");
		if (buf != NULL)
			if (runcmd(buf, tf) < 0)
				break;
	}
}
