#include <inc/mmu.h>
#include <inc/x86.h>
#include <inc/assert.h>
#include <inc/string.h>

#include <kern/pmap.h>
#include <kern/trap.h>
#include <kern/console.h>
#include <kern/monitor.h>
#include <kern/env.h>
#include <kern/syscall.h>
#include <kern/sched.h>
#include <kern/kclock.h>
#include <kern/picirq.h>
#include <kern/cpu.h>
#include <kern/spinlock.h>
#include <kern/sched-head.h>

void divideHndlr();
void debugHndlr();
void nmiHndlr();
void brkptHndlr();
void oflowHndlr();
void boundHndlr();
void illopHndlr();
void deviceHndlr();
void dblfltHndlr();

void tssHndlr();
void segnpHndlr();
void stackHndlr();
void gpfltHndlr();
void pgfltHndlr();

void fperrHndlr();
void alignHndlr();
void mchkHndlr();
void simderrHndlr();
void syscallHndlr();

void clockHndlr();

//void defaultHndlr();

static struct Taskstate ts;

/* For debugging, so print_trapframe can distinguish between printing
 * a saved trapframe and printing the current trapframe and print some
 * additional information in the latter case.
 */
static struct Trapframe *last_tf;

/* Interrupt descriptor table.  (Must be built at run time because
 * shifted function addresses can't be represented in relocation records.)
 */
struct Gatedesc idt[256] = { { 0 } };
struct Pseudodesc idt_pd = {
	sizeof(idt) - 1, (uint32_t) idt
};

static const char *trapname(int trapno)
{
	static const char * const excnames[] = {
		"Divide error",
		"Debug",
		"Non-Maskable Interrupt",
		"Breakpoint",
		"Overflow",
		"BOUND Range Exceeded",
		"Invalid Opcode",
		"Device Not Available",
		"Double Fault",
		"Coprocessor Segment Overrun",
		"Invalid TSS",
		"Segment Not Present",
		"Stack Fault",
		"General Protection",
		"Page Fault",
		"(unknown trap)",
		"x87 FPU Floating-Point Error",
		"Alignment Check",
		"Machine-Check",
		"SIMD Floating-Point Exception"
	};

	if (trapno < sizeof(excnames)/sizeof(excnames[0]))
		return excnames[trapno];
	if (trapno == T_SYSCALL)
		return "System call";
	if (trapno >= IRQ_OFFSET && trapno < IRQ_OFFSET + 16)
		return "Hardware Interrupt";
	return "(unknown trap)";
}

void
trap_init(void)
{
	extern struct Segdesc gdt[];

	// LAB 3: Your code here.

	SETGATE(idt[0], 0, GD_KT, divideHndlr, 0);
	SETGATE(idt[1], 0, GD_KT, debugHndlr, 0);
	SETGATE(idt[2], 0, GD_KT, nmiHndlr, 0);
	SETGATE(idt[3], 0, GD_KT, brkptHndlr, 3);
	SETGATE(idt[4], 0, GD_KT, oflowHndlr, 0);
	SETGATE(idt[5], 0, GD_KT, boundHndlr, 0);
	SETGATE(idt[6], 0, GD_KT, illopHndlr, 0);
	SETGATE(idt[7], 0, GD_KT, deviceHndlr, 0);
	SETGATE(idt[8], 0, GD_KT, dblfltHndlr, 0);

	SETGATE(idt[10], 0, GD_KT, tssHndlr, 0);
	SETGATE(idt[11], 0, GD_KT, segnpHndlr, 0);
	SETGATE(idt[12], 0, GD_KT, stackHndlr, 0);
	SETGATE(idt[13], 0, GD_KT, gpfltHndlr, 0);
	SETGATE(idt[14], 0, GD_KT, pgfltHndlr, 0);

	SETGATE(idt[16], 0, GD_KT, fperrHndlr, 0);
	SETGATE(idt[17], 0, GD_KT, alignHndlr, 0);
	SETGATE(idt[18], 0, GD_KT, mchkHndlr, 0);
	SETGATE(idt[19], 0, GD_KT, simderrHndlr, 0);

	SETGATE(idt[48], 0, GD_KT, syscallHndlr, 3);	

	SETGATE(idt[IRQ_OFFSET + IRQ_CLOCK], 0, GD_KT, clockHndlr, 0);

	// Per-CPU setup 
	trap_init_percpu();

}

// Initialize and load the per-CPU TSS and IDT
void
trap_init_percpu(void)
{
	// Setup a TSS so that we get the right stack
	// when we trap to the kernel.
	ts.ts_esp0 = KSTACKTOP;
	ts.ts_ss0 = GD_KD;

	// Initialize the TSS slot of the gdt.
	gdt[GD_TSS0 >> 3] = SEG16(STS_T32A, (uint32_t) (&ts),
					sizeof(struct Taskstate), 0);
	gdt[GD_TSS0 >> 3].sd_s = 0;

	// Load the TSS selector (like other segment selectors, the
	// bottom three bits are special; we leave them 0)
	ltr(GD_TSS0);

	// Load the IDT
	lidt(&idt_pd);
}


void
clock_idt_init(void)
{
	extern void (*clock_thdlr)(void);
	// init idt structure
	SETGATE(idt[IRQ_OFFSET + IRQ_CLOCK], 0, GD_KT, (int)(&clock_thdlr), 0);
	//SETGATE(idt[IRQ_OFFSET + IRQ_CLOCK], 0, GD_KT, clockHndlr, 0);
	lidt(&idt_pd);
}


void
print_trapframe(struct Trapframe *tf)
{
	cprintf("TRAP frame at %p\n", tf);
	print_regs(&tf->tf_regs);
	cprintf("  es   0x----%04x\n", tf->tf_es);
	cprintf("  ds   0x----%04x\n", tf->tf_ds);
	cprintf("  trap 0x%08x %s\n", tf->tf_trapno, trapname(tf->tf_trapno));
	// If this trap was a page fault that just happened
	// (so %cr2 is meaningful), print the faulting linear address.
	if (tf == last_tf && tf->tf_trapno == T_PGFLT)
		cprintf("  cr2  0x%08x\n", rcr2());
	cprintf("  err  0x%08x", tf->tf_err);
	// For page faults, print decoded fault error code:
	// U/K=fault occurred in user/kernel mode
	// W/R=a write/read caused the fault
	// PR=a protection violation caused the fault (NP=page not present).
	if (tf->tf_trapno == T_PGFLT)
		cprintf(" [%s, %s, %s]\n",
			tf->tf_err & 4 ? "user" : "kernel",
			tf->tf_err & 2 ? "write" : "read",
			tf->tf_err & 1 ? "protection" : "not-present");
	else
		cprintf("\n");
	cprintf("  eip  0x%08x\n", tf->tf_eip);
	cprintf("  cs   0x----%04x\n", tf->tf_cs);
	cprintf("  flag 0x%08x\n", tf->tf_eflags);
	if ((tf->tf_cs & 3) != 0) {
		cprintf("  esp  0x%08x\n", tf->tf_esp);
		cprintf("  ss   0x----%04x\n", tf->tf_ss);
	}
}

void
print_regs(struct PushRegs *regs)
{
	cprintf("  edi  0x%08x\n", regs->reg_edi);
	cprintf("  esi  0x%08x\n", regs->reg_esi);
	cprintf("  ebp  0x%08x\n", regs->reg_ebp);
	cprintf("  oesp 0x%08x\n", regs->reg_oesp);
	cprintf("  ebx  0x%08x\n", regs->reg_ebx);
	cprintf("  edx  0x%08x\n", regs->reg_edx);
	cprintf("  ecx  0x%08x\n", regs->reg_ecx);
	cprintf("  eax  0x%08x\n", regs->reg_eax);
}


static void
trap_dispatch(struct Trapframe *tf)
{
	// Handle processor exceptions.

	switch (tf->tf_trapno) {
		case IRQ_OFFSET + IRQ_SPURIOUS:
			log_printf(0, "Spurious interrupt on irq 7\n");
			// print_trapframe(tf);
			return;
		case IRQ_OFFSET + IRQ_CLOCK:
			cur_tsc = read_tsc();
			setup_cpu_freq();
			current_phase_time += last_interrupt_timing;
			// pic_send_eoi(rtc_check_status());
			sched_yield();
			return;
		case T_PGFLT:
			page_fault_handler(tf);
		case T_BRKPT:
			// ?
			monitor(tf);
		case T_SYSCALL:
			// configure tsc
			// cur_tsc = read_tsc();
 			tf->tf_regs.reg_eax = syscall(
 				tf->tf_regs.reg_eax,
 				tf->tf_regs.reg_edx,
 				tf->tf_regs.reg_ecx,
 				tf->tf_regs.reg_ebx,
 				tf->tf_regs.reg_edi,
 				tf->tf_regs.reg_esi);
 			return;
		default:
			print_trapframe(tf);
			if (tf->tf_cs == GD_KT) {
				panic("unhandled trap in kernel");
			} else {
				env_destroy(curenv);
			}
	}
}

bool in_clk_intr = false;

void
trap(struct Trapframe *tf)
{
	//cprintf("trap():  %d\n",tf->tf_trapno);
		
	// The environment may have set DF and some versions
	// of GCC rely on DF being clear
	asm volatile("cld" ::: "cc");

	// Halt the CPU if some other CPU has called panic()
	extern char *panicstr;
	if (panicstr)
		asm volatile("hlt");

	// Check that interrupts are disabled.  If this assertion
	// fails, DO NOT be tempted to fix it by inserting a "cli" in
	// the interrupt path.
	assert(!(read_eflags() & FL_IF));
	if (tf->tf_trapno != 48)
		log_printf(1, "Incoming TRAP frame at %p, n %u\n", tf, tf->tf_trapno);

	assert(curenv);

	in_clk_intr = (tf->tf_trapno == IRQ_OFFSET + IRQ_CLOCK);




	// Garbage collect if current enviroment is a zombie
	if (curenv->env_status == ENV_DYING) {
		env_free(curenv);
		curenv = NULL;
		sched_yield();
	}

	// Copy trap frame (which is currently on the stack)
	// into 'curenv->env_tf', so that running the environment
	// will restart at the trap point.
	curenv->env_tf = *tf;
	// The trapframe on the stack should be ignored from here on.
	tf = &curenv->env_tf;




	// Record that tf is the last real trapframe so
	// print_trapframe can print some additional information.
	last_tf = tf;

	// Dispatch based on what type of trap occurred
	trap_dispatch(tf);

	// If we made it to this point, then no other environment was
	// scheduled, so we should return to the current environment
	// if doing so makes sense.
	
	//cprintf("trap(): after dispatch\n");

	if (curenv && curenv->env_status == ENV_RUNNING)
		env_run(curenv);
	else
		sched_yield();
}


void
page_fault_handler(struct Trapframe *tf)
{
	//cprintf("page_fault_handler()\n");

	uint32_t fault_va;

	// Read processor's CR2 register to find the faulting address
	fault_va = rcr2();

	// Handle kernel-mode page faults.

	// LAB 3: Your code here.
	if ((tf->tf_cs & (3)) == 0) {
		panic("page fault in kernel mode");
	}

	// We've already handled kernel-mode exceptions, so if we get here,
	// the page fault happened in user mode.

	// Call the environment's page fault upcall, if one exists.  Set up a
	// page fault stack frame on the user exception stack (below
	// UXSTACKTOP), then branch to curenv->env_pgfault_upcall.
	//
	// The page fault upcall might cause another page fault, in which case
	// we branch to the page fault upcall recursively, pushing another
	// page fault stack frame on top of the user exception stack.
	//
	// The trap handler needs one word of scratch space at the top of the
	// trap-time stack in order to return.  In the non-recursive case, we
	// don't have to worry about this because the top of the regular user
	// stack is free.  In the recursive case, this means we have to leave
	// an extra word between the current top of the exception stack and
	// the new stack frame because the exception stack _is_ the trap-time
	// stack.
	//
	// If there's no page fault upcall, the environment didn't allocate a
	// page for its exception stack or can't write to it, or the exception
	// stack overflows, then destroy the environment that caused the fault.
	// Note that the grade script assumes you will first check for the page
	// fault upcall and print the "user fault va" message below if there is
	// none.  The remaining three checks can be combined into a single test.
	//
	// Hints:
	//   user_mem_assert() and env_run() are useful here.
	//   To change what the user environment runs, modify 'curenv->env_tf'
	//   (the 'tf' variable points at 'curenv->env_tf').

	// LAB 4: Your code here.

	if (curenv->env_pgfault_upcall) {

		struct UTrapframe *utf = (struct UTrapframe *)(UXSTACKTOP - sizeof (struct UTrapframe));

		if(tf->tf_esp < UXSTACKTOP && tf->tf_esp >= UXSTACKTOP-PGSIZE) {
                        utf = (struct UTrapframe *)(tf->tf_esp - sizeof (struct UTrapframe) - 4);
                }
		else {
			utf = (struct UTrapframe *)(UXSTACKTOP - sizeof (struct UTrapframe));
		}
		
 		user_mem_assert(curenv, (const void *)utf, sizeof(struct UTrapframe), PTE_W);
		utf->utf_fault_va = fault_va;
		utf->utf_err = tf->tf_err;
		utf->utf_regs = tf->tf_regs;
		utf->utf_eip = tf->tf_eip;
		utf->utf_eflags = tf->tf_eflags;	
		utf->utf_esp = tf->tf_esp;
				
		curenv->env_tf.tf_eip = (uint32_t) curenv->env_pgfault_upcall;
		curenv->env_tf.tf_esp = (uint32_t) utf;
		log_printf(0, "env_run from pgfault\n");
		env_run(curenv);

	}
		


	// Destroy the environment that caused the fault.
	cprintf("[%08x] user fault va %08x ip %08x\n",
		curenv->env_id, fault_va, tf->tf_eip);
	print_trapframe(tf);
	env_destroy(curenv);
}

