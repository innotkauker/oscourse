# JOS #

This repository contains the code I've written for our OS course at CS MSU.
The course was heavily based on [MIT JOS](http://pdos.csail.mit.edu/6.828/2012/index.html),
however, the labs were a bit different.
Besides the labs, I have designed a scheduler able to take
priority change requests from programs,
implementation of which can be found in `kern/sched*` files.

### Who needs this? ###

This is a monument to remind of some long hacking nights
that took place in the fall of 2013.

Who needs monuments, indeed?
