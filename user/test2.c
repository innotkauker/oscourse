#include <inc/lib.h>

void
umain( int argc, char **argv )
{
	int i, j;
	long double p = 1, q = 4.457436;
	// sys_env_set_time(15000); // 20 seconds
	for( j = 0; j < 10; ) {
		for( i = 0; i < 10000; ++i ) {
			p = q*p;
			q = p*1.000000001;
			q *= i/1000.0;
		}
		// sys_env_set_time(10000);
		sys_yield();
		// cprintf("!");
	}
}
