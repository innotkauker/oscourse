/* See COPYRIGHT for copyright information. */

#ifndef JOS_INC_ENV_H
#define JOS_INC_ENV_H

#include <inc/types.h>
#include <inc/trap.h>
#include <inc/memlayout.h>

typedef int32_t envid_t;

// An environment ID 'envid_t' has three parts:
//
// +1+---------------21-----------------+--------10--------+
// |0|          Uniqueifier             |   Environment    |
// | |                                  |      Index       |
// +------------------------------------+------------------+
//                                       \--- ENVX(eid) --/
//
// The environment index ENVX(eid) equals the environment's offset in the
// 'envs[]' array.  The uniqueifier distinguishes environments that were
// created at different times, but share the same environment index.
//
// All real environments are greater than 0 (so the sign bit is zero).
// envid_ts less than 0 signify errors.  The envid_t == 0 is special, and
// stands for the current environment.

#define LOG2NENV		10
#define NENV			(1 << LOG2NENV)
#define ENVX(envid)		((envid) & (NENV - 1))

// Values of env_status in struct Env
enum {
	ENV_FREE = 0,
	ENV_DYING,
	ENV_RUNNABLE,
	ENV_RUNNING,
	ENV_NOT_RUNNABLE
};

// Special environment types
enum EnvType {
	ENV_TYPE_IDLE = 0,
	ENV_TYPE_KERNEL,
	ENV_TYPE_USER
};

// sys_env_*() return values
enum {
	ENV_TIME_OK = 0, // success
	ENV_TIME_LT_ACTIVE = -1, // new time is less then the time process has already spent
	ENV_TIME_BLOCKED = -2, // process is blocked
	ENV_TIME_OF = -3, // the value is too big - _add_time()
	//ENV_TIME_DEFAULT = -4 // the 
};

struct Env {
	struct Trapframe env_tf;	// Saved registers
	struct Env *env_link;		// Next free Env
	// struct Env *next_idle;		// Next env in idle_envs
	// struct Env *prev_idle;		// Previous env in idle_envs
	envid_t env_id;				// Unique environment identifier
	envid_t env_parent_id;		// env_id of this env's parent
	enum EnvType env_type;		// Indicates special system environments
	unsigned env_status;		// Status of the environment
	uint32_t env_runs;			// Number of times environment has run
#ifdef CONFIG_KSPACE
	uint32_t blocking_cycles;
	int static_num;
#else
	// Address space
	pde_t *env_pgdir;		// Kernel virtual address of page dir

	// Exception handling
	void *env_pgfault_upcall;	// Page fault upcall entry point

	// Lab 4 IPC
	bool env_ipc_recving;		// Env is blocked receiving
	void *env_ipc_dstva;		// VA at which to map received page
	uint32_t env_ipc_value;		// Data value sent to us
	envid_t env_ipc_from;		// envid of the sender
	int env_ipc_perm;		// Perm of page mapping received
#endif
	void *loaded_envs_link; // corresponding element in loaded_envs list
	// v variables for new scheduler
	// v times are set in microseconds (usec)
	// v 1 usec = 10^-6 second
	uint64_t assumed_time; 	// running time stated by process
	uint64_t total_time_active; 	// real running time
	uint64_t time_active;
	uint64_t time_waiting; 	// time process is waiting to be run
	bool default_timing; 	// this is set for processes satisfied with our 
						 	// default priority & timing settings
	uint64_t assumed_last_time; // how long the process was assumed to run the previous time
	int penalty; 			// big value here possibly indicates that process is badass
	bool blocked; 			// if we decide that penalty is high, we set this true
				  			// and the process uses default_timing till its death
	bool fast; 				// true if the process is in fast list
};

extern uint64_t started_at_tsc;
extern uint64_t cur_tsc;
extern bool suspicious_behaviour;
extern const int max_penalty; // for blocking

#endif // !JOS_INC_ENV_H
